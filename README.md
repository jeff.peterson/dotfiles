# Setting Up a New Local

A repository for unautomated setup of new computers and backup of important configuration files.

Inspiration for git setup: https://drewdevault.com/2019/12/30/dotfiles.html


### Features
- Minimal bloat (mostly) and fast enough 
- A pretty and functional terminal with command autocompletion and autosuggestions
- Enough neovim setup to be on par with modern GUI text editor look and feel (mostly)
- System-wide keybindings:
    - Move focus of all editors between panes with `^⌘←` `^⌘→` `^⌘↑` `^⌘↓` (cmd + ctrl + left/right/up/down)
    - Cycle tabs within an application with `^⇥` (ctrl + tab)
    - Cycle nested tabs within an application (like neovim tabs) with `⌥⇥` (option + tab)
    - Launch iTerm2 from anywhere with `⌥⌘Space` (cmd + option + space)
    - Swap lines up and down with `⌥↑` `⌥↓` (option + up/down)
- The home directory loosely follows the [XDG Base Directory Specification](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html) to minimize the cruft lying around. Use `~/.local/share`, `~/.local/state`, `~/.config`, and `~/.cache` for organizing system files and directories. 


### Usage

Setup new dotfiles repo:
```sh
cd ~
echo -n "*" > .gitignore
git init
git remote add origin git@<vsc_address>:<account>/dotfiles.git
```

Add new files with:
```sh
git add --force <file_name>
```

The rest of the normal git workflow remains unchanged.

To checkout your dotfiles on a new machine:
```sh
cd ~
git init
git remote add origin git@<vsc_address>:<project>/dotfiles.git 
git fetch
git checkout main
```


### Prerequisites

1. Xcode Command Line Tools `xcode-select --install`
1. Homebrew `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"`


### Brew install

Install all formula and casks in this repo with:
```sh
cat ~/.dotfiles/homebrew/Brewfile | xargs brew install
cat ~/.dotfiles/homebrew/Caskfile | xargs brew install --cask
```



### iTerm2

1. `brew install --cask iterm2` (or use employer's self-service)
1. Install a font with glyphs like MesloLGS NF ([instructions](https://github.com/romkatv/powerlevel10k#meslo-nerd-font-patched-for-powerlevel10k))
1. Specify the preferences directory
    - `defaults write com.googlecode.iterm2 PrefsCustomFolder -string "~/.dotfiles/iterm2/settings"`
1. Tell iTerm2 to use the custom preferences in the directory
    - `defaults write com.googlecode.iterm2 LoadPrefsFromCustomFolder -bool true`
1. Alternative approach: import iTerm2 profile, keybindings, and colors
1. Add custom keybindings
    - In System Preferences > Keyboard > Shortcuts, click "App Shortcuts" (left-side), for `iTerm.app`, add appropriate keybindings for Select Next Tab and Select Previous Tab
    - In System Preferences > Keyboard > Shortcuts, click "App Shortcuts" (left-side), for `iTerm.app`, add appropriate keybindings for Select Pane Left/Right/Below/Above
1. Add custom launch shortcut
    - macOS Monterey and below:
        1. Use Automator.app to create workflow to "Launch Application" iTerm2 called LaunchiTerm2.workflow
        1. In System Preferences > Keyboard > Shortcuts, click "Services" (left-side), add shortcut `⌥⌘Space` to LaunchiTerm2 (right-side)
    - macOS Ventura:
        1. "Services" keyboard shortcuts do not include `.workflow`s anymore so use the Shortcuts application instead``


### Zsh

1. `brew install zsh-syntax-highlighting`
1. `brew install zsh-autosuggestions`
1. `brew install romkatv/powerlevel10k/powerlevel10k`
1. After Docker installation:
    - `ln -s /Applications/Docker.app/Contents/Resources/etc/docker.zsh-completion /usr/local/share/zsh/site-functions/_docker`
    - `ln -s /Applications/Docker.app/Contents/Resources/etc/docker-compose.zsh-completion /usr/local/share/zsh/site-functions/_docker-compose`

Fix the following error
```
zsh compinit: insecure directories, run compaudit for list.
Ignore insecure directories and continue [y] or abort compinit [n]?
```

with the following command
```sh
compaudit | xargs chmod g-w
```

### neovim

1. Install neovim `brew install neovim`
1. Install vim-plug `sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'`
1. Install Plugins `nvim -es -u ~/.config/nvim/init.lua -i NONE -c "PlugInstall" -c "qa"` (doesn't work - need to troubleshoot this someday)
1. Install dependencies:
    1. `brew install fd`
    1. `brew install ripgrep`
    1. `brew install node` (Optional but useful; used by TreeSitter)
    1. `npm install -g neovim`
    1. `pip3 install --user neovim` (See "Python" below before installing)


### VS Code

"Settings Sync" is preferred. Static backups of settings included in the repo as well.

To remove the chime in VS Code when pressing cmd + ctrl + left/right/down, create file `~/Library/KeyBindings/DefaultKeyBinding.dict` with the following contents:
```
{
	"@^\UF701" = "noop:";
	"@^\UF702" = "noop:";
	"@^\UF703" = "noop:";
}
```


### Rectangle

`brew install --cask rectangle` (grant accessibility permissions in System Preferences)

Import `~/.dotfiles/rectangle/config.json` into Rectangle's preferences. 

Note: importing unset keybindings will not unset existing bindings. Remove all default keybindings before importing.


### Python

Python ships with MacOS but is typically a few minor versions behind. Additionally, I feel it's best to just not touch it. There are tools out there like `pyenv` to make version management easy but I do it so rarely that `brew install` @version and `brew link` do the trick.

- `brew install python@3.11` (or whatever version is out now)
- `ln -s /opt/homebrew/bin/python3 ~/bin/python` (homebrew won't sym-link `python` alone)
- `ln -s ~/Library/Python/3.11 ~/Library/Python/Current` (`.zprofile` uses this link to find pip-user-installed executables - update it after changing python versions)
- `pip3 install --user ipython`


### General

- `brew install --cask flycut` (grant accessibility permissions in System Preferences)
- `brew install tree`
- `brew install httpie`
- `brew install jq`
- Add ssh keys for VCS (gitlab.com).
    - Work and personal keys for a single GitLab instance can be added by following [these instructions](https://docs.gitlab.com/ee/user/ssh.html#use-different-accounts-on-a-single-gitlab-instance).
- TODO: add instructions for docker, AWS, k8s, other PaaS, etc etc


TODO: Add notes on ts, bat, rg, and friends

