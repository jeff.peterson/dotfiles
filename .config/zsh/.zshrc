# Enable Powerlevel10k instant prompt
# Initialization code that may require console input must go above this
# block, everything else may go below.
if [[ -r "${XDG_CACHE_HOME}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# use Bash default of kill from from cursor to end, not whole line
bindkey \^U backward-kill-line

# treat path segments as words
WORDCHARS=${WORDCHARS/\/}


# alias better defaults
alias ls="ls -AFG"
alias less="less -FXiRdWK --mouse"
alias grep="grep --color=auto"
alias http="http --print=HBhb"

# alias custom commands
alias ll="ls -al"
alias ts="tree -C -a -L 3 -I \"node_modules|venv|.git|*.pyc\" --dirsfirst"
alias venv="source ./venv/bin/activate"
alias deleteAllDSStore="find . -name \".DS_Store\" -type f -print -delete"
alias givemeuuid="uuidgen | tr -d '\n' | tr '[:upper:]' '[:lower:]' | pbcopy"
alias gitlog="git log --oneline --graph -15"
alias gitpushup="git push -u origin @" 

# alias shortcuts
alias n="nvim"

# alias git shortcuts
alias g="git"
alias ga="git add"
alias gc="git commit"
alias gd="GIT_EXTERNAL_DIFF=difft git diff"
alias gs="git status"

# alias kubernetes shortcuts
alias k="kubectl"
alias kctx="kubectx"
alias kns="kubens"

# alias docker shortcuts
alias d="docker"
alias dc="docker compose"
alias dcu="docker compose up"
alias dcd="docker compose down"
alias dcb="docker compose build"
alias dce="docker compose exec"
alias dcr="docker compose run"
alias dcw="docker compose watch"


# zsh completions

# add brew installed completions
FPATH=$(brew --prefix)/share/zsh-completions:$FPATH
FPATH=$(brew --prefix)/share/zsh/site-functions:$FPATH

# add user installed completions
FPATH=$XDG_DATA_HOME/zsh/site-functions:$FPATH
zstyle ":completion:*:*:docker:*" option-stacking yes

autoload -Uz compinit; compinit
autoload -Uz promptinit; promptinit


# zsh autosuggestions
source $(brew --prefix)/share/zsh-autosuggestions/zsh-autosuggestions.zsh
ZSH_AUTOSUGGEST_BUFFER_MAX_SIZE=20
ZSH_AUTOSUGGEST_STRATEGY=(history completion)
bindkey '^ ' autosuggest-accept


# $PS1 prompt
source $(brew --prefix)/share/powerlevel10k/powerlevel10k.zsh-theme
source $XDG_CONFIG_HOME/p10k/p10k.zsh # To customize prompt, run `p10k configure` or edit p10k.zsh.


# z - jump around
_Z_DATA=$XDG_STATE_HOME/z/.z
source $(brew --prefix)/opt/z/etc/profile.d/z.sh


# fzf - commmand line fuzzy finder
eval "$(fzf --zsh)"


# Docker Desktop
source ~/.docker/init-zsh.sh


# nvm
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"


# zsh syntax highlighting
typeset -A ZSH_HIGHLIGHT_STYLES

ZSH_HIGHLIGHT_STYLES[builtin]='fg=green,bold'
ZSH_HIGHLIGHT_STYLES[command]='fg=green,bold'
ZSH_HIGHLIGHT_STYLES[function]='fg=green,bold'
ZSH_HIGHLIGHT_STYLES[alias]='fg=green,bold'


# Must be last
source $(brew --prefix)/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

