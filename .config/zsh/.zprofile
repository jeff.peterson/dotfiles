# XDG Base Directory Environment Variables
export XDG_DATA_HOME=$HOME/.local/share
export XDG_STATE_HOME=$HOME/.local/state
export XDG_CONFIG_HOME=$HOME/.config
export XDG_CACHE_HOME=$HOME/.cache

# establish $PATH
export PATH=/opt/homebrew/bin:$PATH
export PATH=$PATH:~/.local/bin
export PATH=$PATH:/Users/jeffrey.peterson/Library/Python/Current/bin
export PATH=$PATH:"/Applications/Visual Studio Code.app/Contents/Resources/app/bin"

# home directory cleanup
export LESSHISTFILE=$XDG_STATE_HOME/less/.lesshst
export NODE_REPL_HISTORY=$XDG_STATE_HOME/node/.node_repl_history
export npm_config_cache=$XDG_CACHE_HOME/npm
export PYTHONSTARTUP=$XDG_CONFIG_HOME/python/pythonstartup.py
export IPYTHONDIR=$XDG_STATE_HOME/ipython/
export NVM_DIR=$XDG_CONFIG_HOME/nvm

