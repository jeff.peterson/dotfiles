-- ~/.config/nvim/init.lua

-- NERDTree recommendation: disable netrw at the very start of your init.lua (strongly advised)
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

-- set termguicolors to enable highlight groups
vim.opt.termguicolors = true

vim.opt.mouse = 'a'
vim.opt.number = true

vim.opt.whichwrap:append('<,>,h,l,[,]')

--
-- Extensions
--
-- vim-plug
local Plug = vim.fn['plug#']
-- run ':PlugInstall' after add/edit
vim.call('plug#begin')

-- sensible default settings
Plug 'tpope/vim-sensible'

-- Appearance
-- colorscheme with treesitter support
Plug 'https://gitlab.com/__tpb/monokai-pro.nvim'
-- statusline
Plug 'nvim-lualine/lualine.nvim'
-- statusline icons 
Plug 'kyazdani42/nvim-web-devicons'
-- tabline
Plug 'romgrk/barbar.nvim'
-- highlight line and word
Plug 'yamatsum/nvim-cursorline'

-- Utilities
-- treesitter
Plug('nvim-treesitter/nvim-treesitter', { ['do'] = ':TSUpdate' })
-- nvim-tree
Plug 'kyazdani42/nvim-tree.lua'
-- telescope
Plug 'nvim-lua/plenary.nvim'
Plug('nvim-telescope/telescope-fzf-native.nvim', { ['do'] = 'make' })
Plug('nvim-telescope/telescope.nvim', { ['branch'] = '0.1.x' })
-- keymap helper
Plug 'folke/which-key.nvim'

-- Text editing
-- auto open/close quotes/parens/etc
Plug 'windwp/nvim-autopairs'
-- code commenting
Plug 'numToStr/Comment.nvim'


-- Future
-- Plug 'phaazon/hop.nvim'
-- Plug 'neovim/nvim-lspconfig'
vim.call('plug#end')

--
-- Extension setup
--
-- setup colorscheme
vim.g.monokaipro_filter = 'spectrum'
vim.cmd('colorscheme monokaipro')

-- setup statusline
require('lualine').setup {
    options = { theme = 'monokaipro' }
}

-- setup barbar
require('bufferline').setup()

-- setup nvim-cursorline
require('nvim-cursorline').setup {
    cursorline = { timeout = 50 }
}

-- setup treesitter
require('nvim-treesitter.configs').setup({
    ensure_installed = {
        'python', 'javascript', 'html', 'css', 'json', 'lua', 'markdown', 'sql', 'tsx', 'typescript', 'yaml', 'help', 'vim'
    },
    auto_install = true,
    highlight = { enable = true },
    indent = { enable = true }
})

-- setup nvim-tree
require('nvim-tree').setup({
    git = { ignore = false }
})

-- setup nvim-tree + barbar (tabline)
local nvim_tree_events = require('nvim-tree.events')
local bufferline_api = require('bufferline.api')

local function get_tree_size() return require'nvim-tree.view'.View.width end

nvim_tree_events.subscribe('TreeOpen', function() bufferline_api.set_offset(get_tree_size()) end)
nvim_tree_events.subscribe('Resize', function() bufferline_api.set_offset(get_tree_size()) end)
nvim_tree_events.subscribe('TreeClose', function() bufferline_api.set_offset(0) end)

-- setup WhichKey
local which_key = require('which-key')
which_key.setup()

-- setup Telescope
require('telescope').setup()
require('telescope').load_extension('fzf')

-- setup nvim-autopairs
require('nvim-autopairs').setup()

-- setup Comment
require('Comment').setup()

--
-- Keymaps
--
function map(mode, shortcut, command)
    vim.api.nvim_set_keymap(mode, shortcut, command, { noremap = true, silent = true })
end
function nmap(shortcut, command) map('n', shortcut, command) end
function imap(shortcut, command) map('i', shortcut, command) end
function vmap(shortcut, command) map('v', shortcut, command) end

vim.g.mapleader = ' '

-- Exit Insert mode
imap('jj', '<Esc>')

-- File navigation
nmap('<leader>t', ':NvimTreeFindFileToggle<cr>')
nmap('<leader>ff', '<cmd>Telescope find_files<cr>')
nmap('<leader>fg', '<cmd>Telescope live_grep<cr>')
nmap('<leader>fb', '<cmd>Telescope buffers<cr>')
nmap('<leader>fh', '<cmd>Telescope help_tags<cr>')

-- local builtin = require('telescope.builtin')
-- vim.keymap.set('n', '<leader>ff', builtin.find_files, {})
-- vim.keymap.set('n', '<leader>fg', builtin.live_grep, {})
-- vim.keymap.set('n', '<leader>fb', builtin.buffers, {})
-- vim.keymap.set('n', '<leader>fh', builtin.help_tags, {})

-- Tabline
-- Move to previous/next
nmap('<M-Tab>', '<cmd>BufferNext<cr>')
nmap('<M-S-Tab>', '<cmd>BufferPrevious<cr>')
nmap('<M-w>', '<cmd>BufferClose<cr>')

-- Text editing
nmap('<M-Up>', ':move -2<CR>')
nmap('<M-k>', ':move -2<CR>')
nmap('<M-Down>', ':move +<CR>')
nmap('<M-j>', ':move +<CR>')
imap('<M-Up>', '<Esc>:move -2<CR>')
imap('<M-k>', '<Esc>:move -2<CR>')
imap('<M-Down>', '<Esc>:move +<CR>')
imap('<M-j>', '<Esc>:move +<CR>')
vmap('<M-Up>', ":move '<-2<CR>gv=gv")
vmap('<M-k>', ":move '<-2<CR>gv=gv")
vmap('<M-Down>', ":move '>+1<CR>gv=gv")
vmap('<M-j>', ":move '>+1<CR>gv=gv")

which_key.add({
  { '<leader>f', group = 'file' },
})

